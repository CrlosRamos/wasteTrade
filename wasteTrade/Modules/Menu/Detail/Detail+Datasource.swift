//
//  Detail+Datasource.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/16/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        switch  row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Detail", for: indexPath) as? DetailCell else {return DetailCell()}
            return cell
        default:
            return  UITableViewCell()
        }
    }
}
