//
//  DetailViewController.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 4/15/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var Headerview : UIView!
    
    private let Headerheight : CGFloat = 200
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var footerView: UIView{
            let view = UIView()
            view.backgroundColor = .clear
            return view
        }
        
        tableView.tableFooterView = footerView
        UpdateView()
    }
    
    func UpdateView() {
        tableView.backgroundColor = UIColor.white
        Headerview = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.addSubview(Headerview)
        
        let newheight = Headerheight
        tableView.contentInset = UIEdgeInsets(top: newheight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -newheight)
        
        self.Setupnewview()
    }
    
    func Setupnewview() {
        let newheight = Headerheight
        var getheaderframe = CGRect(x: 0, y: -newheight, width: tableView.bounds.width, height: Headerheight)
        if tableView.contentOffset.y < newheight
        {
            getheaderframe.origin.y = tableView.contentOffset.y
            getheaderframe.size.height = -tableView.contentOffset.y
           
        }

        Headerview.frame = getheaderframe
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tableView.decelerationRate = UIScrollViewDecelerationRateFast
    }
    
    private var previousStatusBarHidden = false
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.Setupnewview()
        if previousStatusBarHidden != shouldHideStatusBar {
            UIView.animate(withDuration: 0.2, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
            })
            previousStatusBarHidden = shouldHideStatusBar
        }
    }
    
    private var shouldHideStatusBar: Bool {
        //** Here’s where we calculate if our text container
        // is going to hit the top safe area
        let frame = tableView.bounds
        return frame.origin.y > view.safeAreaInsets.top
    }
    
    //MARK: — Status Bar Appearance
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        //** We use the slide animation because it works well with scrolling
        return .slide
    }
    override var prefersStatusBarHidden: Bool {
        return shouldHideStatusBar
    }

}
