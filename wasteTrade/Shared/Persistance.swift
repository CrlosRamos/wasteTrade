//
//  Persistance.swift
//  wasteTrade
//
//  Created by Carlos Ramos on 8/20/18.
//  Copyright © 2018 Synappsis. All rights reserved.
//
import KeychainSwift

class Persistance{
    let keychain = KeychainSwift()
    
    func setToken(data:String){
        keychain.set(data, forKey: "token")
    }
    
    func getToken() -> String?{
        return keychain.get("token")
    }
    
    func deleteToken() {
        keychain.delete("token")
    }
}
